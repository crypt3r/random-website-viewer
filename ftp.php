<?php
$url = array(
 'ftp://data.gorgonsteel.com/',
 'ftp://ftp.uninett.no/',
 'ftp://ftp.uio.no/',
 'ftp://ftp.cdc.gov/',
 'ftp://ftp.cdc.gov/',
 'ftp://ftp.ee.lbl.gov/',
 'ftp://ftp.ee.lbl.gov/',
 'ftp://ftp.esd.ornl.gov/',
 'ftp://ftp.fcc.gov/',
 'ftp://ftp.fnal.gov/',
 'ftp://ftp.lanl.gov/',
 'ftp://ftp.loc.gov/',
 'ftp://ftp.ncdc.noaa.gov/',
 'ftp://ftp.aist-nara.ac.jp/',
 'ftp://ftp.casio.co.jp/',
 'ftp://ftp.debian.or.jp/',
 'ftp://ftp.fujixerox.co.jp/',
 'ftp://ftp.iij.ad.jp/',
 'ftp://ftp.astro.uio.no/',
 'ftp://ftp.ntnu.no/',
 'ftp://ftp.inet.no/',
 'ftp://ftp.norut.no/',
 'ftp://ftp.nersc.no',
 'ftp://ftp.nethelp.no/',
 'ftp://ftp.nr.no/',
 'ftp://ftp.ntnu.no/',
 'ftp://ftp.opera.no/',
 'ftp://ftp.powertech.no/',
 'ftp://ftp.skolelinux.no/',
 'ftp://ftp.slackware.no/',
 'ftp://ftp.uib.no/',
 'ftp://ftp.uit.no/',
 'ftp://crydee.sai.msu.ru/',
 'ftp://dozen.mephi.ru/',
 'ftp://ftp.aanet.ru/',
 'ftp://ftp.abbyy.ru/',
 'ftp://ftp.acer.ru/',
 'ftp://ftp.acmetelecom.ru/',
 'ftp://ftp.aha.ru/',
 'ftp://ftp.auckland.ac.nz/',
 'ftp://ftp.mcs.vuw.ac.nz/',
 'ftp://ftp.gathering.org/',
 'ftp://ftp.cmf.nrl.navy.mil/',
 'ftp://ftp.aopen.com.cn/',
 'ftp://ftp.cc.ac.cn/',
 'ftp://ftp.aao.gov.au/',
 'ftp://ftp.adelaide.edu.au/',
 'ftp://ftp.atnf.csiro.au/',
 'ftp://ftp.austlii.edu.au/',
 'ftp://ftp.cse.unsw.edu.au/',
 'ftp://ftp.highway1.com.au/',
 'ftp://ftp.eterna.com.au/',
 'ftp://ftp.ips.gov.au/',
 'ftp://ftp.jamsoft.com.au/',
 'ftp://ftp.jcu.edu.au/',
 'ftp://ftp.marine.csiro.au/',
 'ftp://ftp.is.co.za/',
 'ftp://ftp.sun.ac.za/',
 'ftp://ftp.ula.ve/',
 'ftp://ftp.sudo.ws/',
 'ftp://ftp.ula.ve/',
 'ftp://asftp.aopen.com.tw/',
 'ftp://ftp.aopen.com.tw/',
 'ftp://ftp.cc.ntut.edu.tw/',
 'ftp://ftp.csie.nctu.edu.tw/',
 'ftp://ftp.fic.com.tw/',
 'ftp://ftp.infortrend.com.tw/',
 'ftp://ftp.wavelink.com.tw/',
 'ftp://ftp.upmost.com.tw/',
 'ftp://ftp.itu.edu.tr/',
 'ftp://ftp.tcl.tk/',
 'ftp://ftp.asia.co.th/',
 'ftp://ftp.eng.cmu.ac.th/',
 'ftp://ftp.nectec.or.th/',
 'ftp://ccsoan.irkutsk.su/',
 'ftp://ddt.demos.su/',
 'ftp://ftp.jet.msk.su/',
 'ftp://ftp.sai.msu.su/',
 'ftp://ftp.elf.stuba.sk/',
 'ftp://ftp.r-net.sk/',
 'ftp://ftp.sac.sk/',
 'ftp://arnold.c64.org/',
 'ftp://c64.rulez.org/',
 'ftp://ftp.3gpp.org/',
 'ftp://ftp.aaas.org/',
 'ftp://ftp.acornusers.org/',
 'ftp://ftp.afterstep.org/',
 'ftp://ftp.alsa-project.org/',
 'ftp://ftp.am-utils.org/',
 'ftp://ftp.amigascne.org/',
 'ftp://ftp.ams.org/',
 'ftp://ftp.amsat.org/',
 'ftp://ftp.vim.org/',
 );
$i = rand(0, count($url)-1);
$selectedurl = $url[$i];
?>
<head>
	<title>Load a random FTP</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link href="/assets/bootstrap.min.css" rel="stylesheet">
    <link href="/assets/ie10-viewport-bug-workaround.css" rel="stylesheet">
    <link href="/assets/dashboard.css" rel="stylesheet">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
	<div class="container">
		<br>
		<div class="navbar-fixed-top">
			<center>
				<a style="border-radius: 0px; !important" class="btn btn-danger" target="_blank" href="<?php echo $selectedurl; ?>" role="button"><?php echo $selectedurl; ?></a>
				<a style="border-radius: 0px; !important" class="btn btn-danger" href="ftp" role="button">Load a new FTP server</a>
			</center>
		</div>
	</div>
<body style="margin:0px;padding:0px;overflow:hidden">
    <iframe src="<?php echo $selectedurl; ?>" frameborder="0" style="overflow:hidden;overflow-x:hidden;overflow-y:hidden;height:100%;width:100%;position:absolute;top:0px;left:0px;right:0px;bottom:0px" height="100%" width="100%"></iframe>
</body>